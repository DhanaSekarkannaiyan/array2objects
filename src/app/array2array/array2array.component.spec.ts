import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Array2arrayComponent } from './array2array.component';

describe('Array2arrayComponent', () => {
  let component: Array2arrayComponent;
  let fixture: ComponentFixture<Array2arrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Array2arrayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Array2arrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
