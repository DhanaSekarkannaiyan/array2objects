import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Array2arrayComponent } from './array2array/array2array.component';
const routes: Routes = [
  {path:'Array2array',component:Array2arrayComponent},
  {path:'',redirectTo:'/Array2array',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
